<?php
/**
 *  @file
 *    Admin panel forms
 */

// These defines are just temporary until the various services can be
// reintegrated into the Crowd module 2.x branch.
define('CROWD_REST_CLASS', TRUE);
define('CROWD_SOAP_CLASS', FALSE);
define('CROWD_LDAP_CLASS', FALSE);

/**
 * Set the global client object so the admin pages can access it
 */
//Include the enabled Crowd API classes and set the global object
if( variable_get('crowd_service_api', '') == 'SOAP') {
  include_once(drupal_get_path('module', 'crowd') . '/crowd_soap_service.class.php');
  global $_crowd_client;
  $_crowd_client = new crowd_soap_client();
}
if( variable_get('crowd_service_api', '') == 'LDAP') {
  include_once(drupal_get_path('module', 'crowd') . '/crowd_ldap_service.class.php');
  global $_crowd_client;
  $_crowd_client = new crowd_ldap_client();
}
if( variable_get('crowd_service_api', '') != 'SOAP' || variable_get('crowd_service_api', '') != 'LDAP') {
  include_once(drupal_get_path('module', 'crowd') . '/crowd_rest_service.class.php');
  global $_crowd_client;
  $_crowd_client = new crowd_rest_client();
}

/**
 * Menu callback function to generate the module configuration form
 * @return
 *   Module configuration form
 */
function crowd_admin_settings() {
  //Service API Settings
  $form['service'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service API'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $options = array();
  (CROWD_REST_CLASS) ? $options['REST'] = t('REST Service (crowd 2.1 and later)') : NULL;
  (CROWD_SOAP_CLASS) ? $options['SOAP'] = t('SOAP Service') : NULL;
  (CROWD_LDAP_CLASS) ? $options['LDAP'] = t('LDAP Service') : NULL;
  $form['service']['crowd_service_api'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose the service API that crowd will use to connect to the crowd server'),
    '#description' => t('By default the REST service is used'),
    '#options' => $options,
    '#default_value' => variable_get('crowd_service_api', array('REST' => 'REST')),
  );
  //Server settings
  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Crowd server settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['server']['crowd_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Crowd server'),
    '#default_value' => variable_get('crowd_server', ''),
    '#size' => 30,
    '#maxlength' => 55,
    '#description' => t('Location of Atlassian Crowd authentication service.'),
  );

  $form['server']['crowd_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Crowd port'),
    '#default_value' => variable_get('crowd_port', '443'),
    '#size' => 30,
    '#maxlength' => 8,
    '#description' => t('443 is the standard ssl port. 8443 is the standard non-root port for Tomcat.'),
  );

  $form['server']['crowd_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Crowd URI'),
    '#default_value' => variable_get('crowd_uri', ''),
    '#size' => 30,
    '#description' => t('If Crowd is not at the root of the host, include a URI (e.g., /crowd/rest/usermanagement/latest).'),
  );

  $form['server']['crowd_application'] = array(
    '#type' => 'textfield',
    '#title' => t('Crowd Application Name'),
    '#default_value' => variable_get('crowd_application', ''),
    '#size' => 30,
    '#description' => t('Applications need to be registered with Crowd in order to provide SSO services.  Enter the name of your registered application here.'),
  );

  $form['server']['crowd_application_password'] = array(
    //'#type' => 'password',
    '#type' => 'textfield',
    '#title' => t('Crowd Application Password'),
    '#default_value' => variable_get('crowd_application_password', ''),
    '#size' => 30,
    '#description' => t('Enter the password used by your application to authenticate with Crowd'),
  );
  //Cookie Settings
  $form['server']['cookie_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('SSO Cookie Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['server']['cookie_options']['crowd_cookie_sso_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Crowd Cookie SSO Domain'),
    '#default_value' => variable_get('crowd_cookie_sso_domain', ''),
    '#size' => 30,
    '#description' => t('This is the domain that Crowd will generate SSO cookies for.'),
  );

  $form['server']['cookie_options']['crowd_secure_sso_cookie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Secure SSO Cookie'),
    '#default_value' => variable_get('crowd_secure_sso_cookie', FALSE),
    '#description' => t('If checked, the "Secure" flag is set on the cookie. This will break SSO for applications not accessed over SSL/TLS (https://), potentially making logging into Crowd impossible.'),
  );

  $form['server']['cookie_options']['crowd_logout_no_cookie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log out if cookie is deleted?'),
    '#default_value' => variable_get('crowd_logout_no_cookie', FALSE),
    '#description' => t('If enabled, this option will automatically log the current user out of Drupal if the Crowd SSO cookie has been deleted.  User 1 is exempted from this behavior.'),
  );

  $form['server']['cookie_options']['crowd_validate_token'] = array(
    '#type' => 'checkbox',
    '#title' => t('Validate the SSO auth token on each request?'),
    '#default_value' => variable_get('crowd_validate_token', FALSE),
    '#description' => t('If enabled, this option will validate the SSO auth token against Crowd on every page load.  ') .
      '<b>' . t('WARNING:') . '</b> ' .
      t('enabling this option may negatively impact performance of your web server and Crowd server significantly.  Use with caution!'),
  );
  /**
   * Crowd to Drupal mapped groups
   */
    $form['groups'] = array(
      '#type' => 'fieldset',
      '#title' => t('Current Group Map'),
      '#description' => t('These are the currently mapped CROWD groups to Drupal roles.<br><br>'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $group_map = variable_get('crowd_groups', array());
    $site_roles = user_roles();
    $form['groups']['key'] = array(
      '#prefix' => '<div id="group-map-key">',
      '#markup' => t('<strong>Crowd Group&nbsp;&nbsp;&nbsp;Drupal Role</strong>'),
      '#suffix' => '</div>',
     );
    if ( !empty ($group_map)) {
      foreach ( $group_map as $key => $value) {
        if ( $value != 0) {
         $form['groups'][$key] = array(
           '#prefix' => '<div id="group-map-' . $key . '">',
           '#markup' => '[' . $key . '] => ' . $site_roles[$value],
           '#suffix' => '</div>'
         );
        }
      }
    }
    $form['groups']['groups-page-link'] = array(
      '#prefix' => '<div id="link-to-crowd-groups-page">',
      '#value' => '<br>' . l(t('Crowd Group Mapping Page'), 'admin/settings/crowd/group-mapping', array('attributes' => array('rel' => '_blank'))),
      '#suffix' => '</div>',
    );

    // Give the option to fall back on local logins
    $form['other-settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Other Crowd Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['other-settings']['crowd_allow_local_logins'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow local logins if Crowd fails'),
      '#default_value' => variable_get('crowd_allow_local_logins', TRUE)
    );

  /**
   * Include LDAP configuration settings if the LDAP Auth module is enabled
   */
  if ( module_exists('ldapauth') && CROWD_LDAP_CLASS) {
    $form['ldap'] = array(
      '#type' => 'fieldset',
      '#title' => t('LDAP settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['ldap']['crowd_useldap'] = array(
      '#type' => 'checkbox',
      '#title' => t('Should we extract the user email from an LDAP directory?'),
      '#default_value' => variable_get('crowd_useldap', 0),
      '#description' => t('Activate this option if you want to extract the user email from an LDAP directory. <strong>Ldapauth module must be enabled and configured</strong>.'),
    );

    $form['ldap']['crowd_ldap_email_attribute'] = array(
      '#type' => 'textfield',
      '#title' => t('Email attribute'),
      '#default_value' => variable_get('crowd_ldap_email_attribute', 'mail'),
      '#size' => 30,
      '#maxlength' => 55,
      '#description' => t('LDAP entry attribute containing the email address.'),
    );

    /**
     * Include LDAP Groups configuration settings if the LDAP Groups module is enabled
     */
    if ( module_exists('ldapgroups')) {
      $form['ldap']['crowd_useldap_groups'] = array(
        '#type' => 'checkbox',
        '#title' => t('Should we extract user groups from an LDAP directory?'),
        '#default_value' => variable_get('crowd_useldap_groups', 0),
        '#description' => t('Activate this option if you want to extract the user groups from an LDAP directory. <strong>Ldapgroups module must be enabled and configured</strong>.'),
      );
    }
  }
  return system_settings_form($form);
}

/**
 * This admin page allows the drupal site admin to assign Drupal roles based on
 * crowd groups.
 */
function crowd_group_mapping() {
  global $_crowd_client;

  try {
    $groups = $_crowd_client->list_groups();
  } catch(Exception $e) {
    drupal_set_message('Crowd failed to connect.');
  }
  $roles = user_roles(TRUE);
  if ( !empty($groups) && $groups != FALSE && !empty($roles)) {
    $options = array('0' => 'Select a role');
    foreach ($roles as $key => $role) {
      $options[$key] = $role;
    }
    $defaults = variable_get('crowd_groups', array());
    $form['groups'] = array(
      '#type' => 'fieldset',
      '#title' => t('Crowd Group Mapping'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => 'For each crowd group select a coresponding Drupal role'
    );
    $form['groups']['crowd_groups'] = array(
      '#tree' => TRUE
    );
    foreach ($groups as $key => $value) {
      $form['groups']['crowd_groups'][$value] = array(
        '#type' => 'select',
        '#title' => $value,
        '#options' => $options,
        '#default_value' => (isset($defaults[$value])) ? $defaults[$value] : array('0' => 'Select a role'),
      );
    }
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save configuration'
    );

  }
  elseif (!empty($groups) && empty($roles)) {
    $message = '</p>' . t("No Drupal roles defined. Please add some roles and try again") . "</p>"; 
  }
  else{
    $message = '<p>' . t("Couldn't connect to crowd server, please check your connection settings and try again.  Messages: ") . serialize($groups) . '</p>';

  }
  if ( !isset($form)) {
    $form['message'] = array(
      '#markup' => $message
    );
  }
  $form['#theme'] = 'system_settings_form';
  return $form;
}
function crowd_group_mapping_submit($form, $form_state) {
  variable_del('crowd_groups');
  variable_set('crowd_groups', $form_state['values']['crowd_groups']);
  drupal_set_message(t('Configuration options saved'));
}
