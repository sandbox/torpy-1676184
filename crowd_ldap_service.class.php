<?php
// If the ldapauth module is enabled, provide integration
define('CROWD_USELDAP', variable_get('crowd_useldap', 0));
define('CROWD_USELDAP_GROUPS', variable_get('crowd_useldap_groups', 0));
define('LDAPAUTH_LOGIN_PROCESS', variable_get('ldapauth_login_process', 0));
define('LDAPAUTH_LOGIN_CONFLICT', variable_get('ldapauth_login_conflict', 1));
define('CROWD_LDAP_EMAIL_ATTR', variable_get('crowd_ldap_email_attribute', 'mail'));

function ldap_groups_add(){
  if(CROWD_USELDAP) {
    require_once(drupal_get_path('module', 'ldapgroups') .'/ldapgroups.inc');
    global $_ldapauth_ldap;
    // Cycle through LDAP configurations.  First one to succeed wins.
    $result = db_query("SELECT sid FROM {ldapauth} WHERE status = 1 ORDER BY weight");
    while ($row = db_fetch_object($result)) {
      // Initialize LDAP.
      if (!_ldapauth_init($row->sid)) {
        continue;
      }
    }
    /**
     * The LDAP Integration module stores LDAP connection information in the user object, and will not work if
     * this data is not set up properly.  This is performed in ldapauth.module, which this module overrides.
     */
    $ldap_user = _ldapauth_user_lookup($crowd_user->name);
    $info['ldap_authentified'] = TRUE;
    $info['ldap_dn'] = $ldap_user['dn'];
    $info['ldap_config'] = $_ldapauth_ldap->getOption('sid');
    $user->ldap_config = $_ldapauth_ldap->getOption('sid');
    // Import the user groups as roles if configured to do so
    if(CROWD_USELDAP_GROUPS) {
      ldapgroups_user_login($user);
    }
  }
  // Update the user profile based on LDAP information
  if(CROWD_USELDAP) {
    // Retrieve the LDAP mappings for profile fields and attributes
    $ldap_drupal_reverse_mappings = _ldapdata_reverse_mappings(variable_get('ldapprov_server', 0));
    // Retrieve profile fields list.
    $profile_fields = _ldapdata_retrieve_profile_fields();
    // Build an array of mapped fields for updating the user profile
    if((!empty($profile_fields)) && (!empty($ldap_drupal_reverse_mappings))) {
      foreach ($profile_fields as $key => $field) {
        if (isset($ldap_drupal_reverse_mappings[$key])) {
          foreach($crowd_user->attributes->SOAPAttribute as $attribute) {
            if($attribute->name == $ldap_drupal_reverse_mappings[$key]) {
              $info[$field] = $attribute->values->string;
              break;
            }
          }
        }
      }
    }

    // Only update the profile if there is data to update
    if((module_exists('profile')) && (!empty($info))) {
      // Load the configured profile categories
      $categories = profile_categories();
      if(!empty($categories)) {
        // Loop through the categories
        foreach($categories as $index=> $category) {
          // Retrieve the fields for each category
          $result = _profile_get_fields($category['name'],false);
          $wanted = array();
          while ($profile_field = db_fetch_object($result)) {
            // Only map in the fields for this category
            $wanted[$profile_field->name] = $info[$profile_field->name];
          }
          // Update the user's profile
          profile_save_profile($wanted, $user, $category['name'], false);
        }
      }
    }
  }
}

    //// Either login in or registers the current user, based on username. Either way, the global $user object is populated based on $form_state['values']['name'].
    //if(CROWD_USELDAP) {
    //  user_external_login_register($form_state['values']['name'], 'ldapauth');
    //}
    //else {}