<?php
/**
 * This is a restful service class for the Atlassian SSO solution crowd.
 *
 * @author
 *  John P. Warren
 *
 */

/**
 * The main class
 *
 * Implements the crowd service interface
 */
class crowd_rest_client implements crowd_service{
  //Properties
  private $request_url;
  private $request_headers;
  private $request_method;
  private $request_data;
  private $request_service;

  //Constructor
  function __construct() {
    //Create the full url for drupal_http_request()
    $server_address = variable_get('crowd_server', '');
    if (strpos($server_address, 'http://') !== FALSE) {
      $url_prefix = 'http://';
    } else {
      $url_prefix = 'https://';
    }
    if (!empty($url_prefix)) {
      $url_suffix = str_ireplace($url_prefix, '', $server_address); // ($server_data, $url);
    } else {
      watchdog('crowd', 'Crowd is not configured on this server!', array(), WATCHDOG_ERROR);
      drupal_set_message(t('Crowd is not configured on this server, please contact the site administrator for help.'));
    }
    $this->request_url = $url_prefix . variable_get('crowd_application', '') . ':' . variable_get('crowd_application_password', '') . '@' . $url_suffix . ':' . variable_get('crowd_port', '443') . variable_get('crowd_uri', '');
    $this->request_headers = array('Accept' => 'application/json', 'Content-Type' => 'application/xml');
    $this->request_method = 'POST';
  }

  /**
   * This method authorizes a user through the crowd server
   * @param $username
   *  string crowd username
   * @param $password
   *  string user password
   * @return $return_data
   *  an keyed array with the Crowd token or FALSE
   */
  public function authorize($username, $password) {
    $service = '/session';
    $this->request_data = '<?xml version="1.0" encoding="UTF-8"?>
      <authentication-context>
        <username>' . $username .'</username>
        <password>' . $password . '</password>
        <validation-factors>
          <validation-factor>
            <name>' . $_SERVER['HTTP_HOST'] . '</name>
            <value>' . $_SERVER['SERVER_ADDR'] . '</value>
          </validation-factor>
        </validation-factors>
      </authentication-context>';
    //@todo add explicit json header
    $final_url = $this->request_url . $service;
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => 3,
      'timeout' => 120.0,
    );
    $result = drupal_http_request($final_url, $options);

    if ($result->code == '204' && $result->status_message == 'DELETE') {
      return true;
    }
    if ($result->code == '200' || $result->code == '201') {
      $data = json_decode($result->data);
      $return_data = array('token' => $data->token);
      drupal_set_message(t('Successfully logged @name in through crowd.', array('@name' => $data->user->name)));
      return $return_data;
    } else {
      watchdog('crowd', 'Restful service return http code %code.  Error: %data', array('%code' => $result->code, '%data' => serialize($result)), WATCHDOG_ERROR);
      drupal_set_message('Unable to log into crowd server, check your username/password and try again.', 'error');
      return FALSE;
    }
  }

  public function logout($crowd_token) {
    global $user;
    $service = '/session';
    $final_url = $this->request_url . $service . '/' . $crowd_token;
    $this->request_method = 'DELETE';
    $this->request_data = '';
    // Invalidate the token for this user for all application clients in Crowd.
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => 3,
      'timeout' => 120.0,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '204' && $result->status_message == 'No Content') {
      return TRUE;
    } else {
      watchdog('crowd', t('Logout of crowd failed, messages: @data', array('@data' => serialize($result))), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Get User Object
   *
   * @param $user_token
   *  a string containing the Crowd token
   * @return $crowd_data
   *  an array containing the user data points or FALSE
   */
  public function user_get($user_token) {
    $service = '/session';
    $this->request_method = 'GET';
    $this->request_data = url($user_token, array('query' => array('expand' => 'user')));
    $final_url = $this->request_url . $service . $this->request_data;
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'max_redirects' => 3,
      'timeout' => 120.0,
    );
    $result = drupal_http_request($final_url, $options);
    $this->request_method = 'POST';
    if ($result->code == '200' || $result->code =='201') {
      $crowd_user = json_decode($result->data);
      $crowd_data = array();
      $crowd_data['username'] = $crowd_user->user->name;
      $crowd_data["email"] = $crowd_user->user->email;
      $crowd_data["active"] = $crowd_user->user->active;
      return $crowd_data;
    } else {
      watchdog('crowd', 'Crowd authentication failed to get the user object for user %username. Error: %data', array('%username' => $username, '%data' => serialize($result)), WATCHDOG_ERROR);
      drupal_set_message('There has been a problem with your user status on the Crowd server, please see your Atlassian Crowd server administrator for help.', 'error');
      return FALSE;
    }
  }

  /**
   * This method checks to see if the user is already logged into Crowd
   *
   * @param $user_token
   *  a string containing the Crowd token
   * @return $boolean
   *  TRUE or FALSE
   */
  public function crowd_is_logged_in($user_token) {
    $service = '/session';
    $this->request_method = 'GET';
    $this->request_headers = array('Accept' => 'application/json', 'Content-Type' => 'application/xml');
    $this->request_data = url($user_token);
    $final_url = $this->request_url . $service . $this->request_data;
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'max_redirects' => 3,
      'timeout' => 120.0,
    );
    $result = drupal_http_request($final_url, $options);
    $this->request_method = 'POST';
    $data = json_decode($result->data);
    //var_dump($data);
    //die;
    if (isset($data->user->active) ) { // Should always be true or false
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * This method returns all active groups from the crowd server.
   *
   * @return $result
   *  Returns a serial array of the groupnames.  On failure it logs the $result object and returns FALSE.
   */
  public function list_groups() {
    $service = '/search';
    $function = url('', array('query' => array('entity-type' => 'group')));
    $group_list_xml = '<?xml version="1.0" encoding="UTF-8"?>
    <property-search-restriction>
      <property>
        <name>active</name>
        <type>BOOLEAN</type>
      </property>
      <match-mode>EXACTLY_MATCHES</match-mode>
      <value>true</value>
    </property-search-restriction>';
    $this->request_data = $group_list_xml;
    if ($this->request_method != 'POST') {
      $this->request_method = 'POST';
    }
    $final_url = $this->request_url . $service . str_ireplace('/', '', $function);
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => 3,
      'timeout' => 120.0,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='201') {
      $data = json_decode($result->data);
      $results = array();
      foreach($data->groups as $key => $value) {
        $results[] = $value->name;
      }
      return $results;
    } else {
      watchdog('crowd', 'Crowd group listing failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      return FALSE;
    }
    return NULL;
  }
  /**
   * This method gets the crowd groups for the current user
   *
   * @param $name
   *  The crowd username
   * @return $user_crowd_groups
   *  an array containing all of the given users groups
   */
  public function list_user_groups($name) {
    $service = '/user/group/nested';
    $url_query = str_ireplace('/', '', url('', array('query' => array('username' => $name))));
    $this->request_data .= '';
    if ($this->request_method != 'GET') {
      $this->request_method = 'GET';
    }
    $final_url = $this->request_url . $service . $url_query;
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => 3,
      'timeout' => 120.0,
    );
    $result = drupal_http_request($final_url, $options);
    $this->request_method = 'POST';
    if ($result->code == '200' || $result->code =='201') {
      $user_crowd_data = json_decode($result->data);
      $user_crowd_groups = array();
      foreach($user_crowd_data->groups as $key => $value) {
        $user_crowd_groups[] = $value->name;
      }
      return $user_crowd_groups;
    } else {
      watchdog('crowd', 'Crowd user group listing failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      drupal_set_message('There has been a problem with your user status on the Crowd server, please see your Atlassian Crowd server administrator for help.', 'error');
      return FALSE;
    }
    return NULL;
  }
}