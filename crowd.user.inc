<?php
/**
 *  @file
 *    This file contains some forms and functions to facilitate crowd users on
 *    Drupal sites with some useful tools.
 */

 function crowd_user_toolbox() {
    if ( isset( $_COOKIE['crowd_token_key'])) {
      $auth_token = $_COOKIE['crowd_token_key'];
      $crowd_client = new crowd_rest_client();
      $crowd_user = $crowd_client->user_get($auth_token);
      $form['toolbox'] = array(
        '#type' => 'fieldset',
        '#title' => t('Crowd User Tools'),
        '#collapsible' => FALSE,
      );
      $form['toolbox']['crowd_user_data'] = array(
        '#type' => 'fieldset',
        '#title' => t('User data from Crowd'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['toolbox']['crowd_user_data']['crowd_username'] = array(
        '#prefix' => '<div id="crowd-username">',
        '#markup' => 'Username = ' . $crowd_user['username'],
        '#suffix' => '</div>',
      );
      $form['toolbox']['crowd_user_data']['crowd_email'] = array(
        '#prefix' => '<div id="crowd-email">',
        '#markup' => 'Email = ' . $crowd_user['email'],
        '#suffix' => '</div>',
      );
      $form['toolbox']['crowd_local_roles'] = array(
        '#type' => 'fieldset',
        '#title' => t('Locally assigned roles'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );

      global $user;

      foreach ($user->roles as $key => $value) {
        $form['toolbox']['crowd_local_roles'][$key] = array(
        '#prefix' => '<div class="crowd-local-roles">',
        '#markup' => 'Role = ' . $value,
        '#suffix' => '</div>',
      );
      }
      $form['toolbox']['refresh_data_from_crowd'] = array(
        '#type' => 'submit',
        '#value' => t('Refresh Information from crowd.'),
      );
      return $form;
    }
    else {
      drupal_set_message(t('You are not logged into Crowd and therefore cannot access these tools.'), 'error');
      $form['toolbox'] = array(
        '#type' => 'fieldset',
        '#title' => t('Crowd User Tools'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      return $form;
    }
 }
 function crowd_user_toolbox_validate($form, &$form_state) {}
 function crowd_user_toolbox_submit($form, &$form_state) {
  if ( isset( $_COOKIE['crowd_token_key'])) {
      $auth_token = $_COOKIE['crowd_token_key'];
      $crowd_client = new crowd_rest_client();
      $crowd_user = $crowd_client->user_get($auth_token);
      _crowd_update_user($crowd_user);
  }
  else {
   drupal_set_message(t('You are not logged into Crowd and therefore cannot access these tools.'), 'error'); 
  }
 }