<?php
/**
 * Crowd SOAP API
 */
//Includes
include_once(drupal_get_path('module', 'crowd') . '/Services_Atlassian_Crowd-0.9.5/Crowd.php');
//include_once

/**
 * Helper function to create a new instance of the Services_Atlassian_Crowd object
 *
 * @see Services_Atlassian_Crowd::__construct()
 */
function soap_crowd_get_client() {
  $crowd_server = trim(variable_get('crowd_server', ''), '/');

  if(preg_match("/^https\:/", $crowd_server)) {
    /**
     * Define the path to the cached WSDL file
     * @see crowd_check_wsdl()
     */
    $wsdl = file_directory_path().DIRECTORY_SEPARATOR.'crowdsoapservice.wsdl';

    if(!file_exists($wsdl)){
      // Display an error
      drupal_set_message(t('Unable to connect to Atlassian Crowd. Please contact your system administrator.'), 'error');
      watchdog('crowd', 'WSDL file has not been cached. You must cache the WSDL from the Crowd server; please visit %url',
        array('%url' => url('admin/settings/crowd/cache-wsdl')),
        WATCHDOG_CRITICAL);
      return false;
    }
  }
  else {
    $wsdl = $crowd_server.':'.
      variable_get('crowd_port', '443').'/'.
      variable_get('crowd_uri', '');
  }

  // Define the configuration array to pass to the Services_Atlassian_Crowd constructor
  $config = array(
    'app_name' => variable_get('crowd_application', ''),
    'app_credential' => variable_get('crowd_application_password', ''),
    'service_url' => $wsdl,
  );

  // Create a new instance of the Crowd SOAP client
  try {
    $crowd = new Services_Atlassian_Crowd($config);
  }
  catch(Exception $e) {
    watchdog('crowd', 'Cannot instantiate Crowd object, exception thrown: %exception', array('%exception' => $e->getMessage()), WATCHDOG_ERROR);
    return false;
  }

  if(!is_object($crowd)) {
    watchdog('crowd', 'Failed to connect to Atlassian Crowd', null, WATCHDOG_ERROR);
    return false;
  }
  else {
    // Authenticate the Drupal application in Crowd
    try {
      $server_token = $crowd->authenticateApplication();
      if($server_token) {
        return $crowd;
      }
      else {
        drupal_get_messages('error');
        watchdog('crowd', 'Failed to authenticate this application', null, WATCHDOG_ERROR);
      }
    }
    catch(Exception $e) {
      // Remove any error messages that may have been thrown by the Services_Atlassian_Crowd object and fail silently
      drupal_get_messages('error');
      watchdog('crowd', 'Failed to authenticate this application, exception thrown: %exception', array('%exception' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }
  return false;
}

/**
 * Helper function to retrieve a Crowd-authenticated user object from Crowd
 * @param $user_token
 *   Authentication token string stored in a cookie by Crowd
 * @return
 *   Returns the user object from Crowd, or false
 */
function soap_crowd_get_user($user_token) {
  // Get the Crowd connector instance
  $crowd = crowd_get_client(true);
  try {
    // Load the Crowd-authenticated user using the Crowd authentication token as the search parameter
    $result = $crowd->__call('findPrincipalByToken', array($user_token));
    if(!$result) {
      watchdog('crowd', 'Failed to load the user by token from Crowd.', null, WATCHDOG_ERROR);
      return false;
    }
    return $result;
  }
  catch(Exception $e) {
    watchdog('crowd', 'Exception thrown when loading the user by token - %message.', array('%message' => $e->getMessage()), WATCHDOG_ERROR);
    return false;
  }
  return false;
}
/**
 * Helper function to determine if the current user is already logged in to Crowd.
 * @return
 *   Either returns the user's SSO token, or false if the cookie wasn't found.
 */
function soap_crowd_is_logged_in() {
  // Get the Crowd SSO cookie.
  $crowd_token = $_COOKIE['crowd_token_key'];

  // If the SSO cookie isn't defined, then assume the user does not have a valid session.
  if (!empty($crowd_token)) {
    $crowd = crowd_get_client();
    if($crowd) {
      try{
        $result = $crowd->__call('isValidPrincipalToken', array(
          $crowd_token,
          $_SERVER['HTTP_USER_AGENT'],
          $_SERVER['REMOTE_ADDR'],
        ));
        if(!$result) {
          // Expire the Crowd SSO cookie
          setcookie('crowd.token_key', '', time()-42000, '/', CROWD_SSO_DOMAIN, CROWD_SECURE_SSO_COOKIE);
          return false;
        }
        return $crowd_token;
      }
      catch(Exception $e) {
        watchdog('crowd', 'Failed to verify user token.  Exception thrown: %exception', array('%exception' => $e->getMessage()), WATCHDOG_ERROR);
      }

    }
  }
  else {
    return false;
  }
}

/**
 * Helper function to update newly created user accounts in Drupal.  This is called when:
 * - a user successfully authenticates against Crowd and the user account doesn't exist in Drupal
 *
 * @param $crowd_user
 *   Object containing user data from Crowd
 */
function _soap_crowd_update_user($crowd_user) {
  global $user;
  $mail = '';
  // Extract the mail attribute
  foreach($crowd_user->attributes->SOAPAttribute as $attribute) {
    if($attribute->name == CROWD_LDAP_EMAIL_ATTR) {
      $mail = $attribute->values->string;
      break;
    }
  }

  // Update the user's email in Drupal with data from Crowd
  $info = array(
    'mail' => $mail
  );
  if(CROWD_USELDAP) {
    require_once(drupal_get_path('module', 'ldapgroups') .'/ldapgroups.inc');
    global $_ldapauth_ldap;
    // Cycle through LDAP configurations.  First one to succeed wins.
    $result = db_query("SELECT sid FROM {ldapauth} WHERE status = 1 ORDER BY weight");
    while ($row = db_fetch_object($result)) {
      // Initialize LDAP.
      if (!_ldapauth_init($row->sid)) {
        continue;
      }
    }
    /**
     * The LDAP Integration module stores LDAP connection information in the user object, and will not work if
     * this data is not set up properly.  This is performed in ldapauth.module, which this module overrides.
     */
    $ldap_user = _ldapauth_user_lookup($crowd_user->name);
    $info['ldap_authentified'] = TRUE;
    $info['ldap_dn'] = $ldap_user['dn'];
    $info['ldap_config'] = $_ldapauth_ldap->getOption('sid');
    $user->ldap_config = $_ldapauth_ldap->getOption('sid');
    // Import the user groups as roles if configured to do so
    if(CROWD_USELDAP_GROUPS) {
      ldapgroups_user_login($user);
    }
  }
  $user = user_save($user, $info, 'account');

  // Update the user profile based on LDAP information
  if(CROWD_USELDAP) {
    // Retrieve the LDAP mappings for profile fields and attributes
    $ldap_drupal_reverse_mappings = _ldapdata_reverse_mappings(variable_get('ldapprov_server', 0));
    // Retrieve profile fields list.
    $profile_fields = _ldapdata_retrieve_profile_fields();
    // Build an array of mapped fields for updating the user profile
    if((!empty($profile_fields)) && (!empty($ldap_drupal_reverse_mappings))) {
      foreach ($profile_fields as $key => $field) {
        if (isset($ldap_drupal_reverse_mappings[$key])) {
          foreach($crowd_user->attributes->SOAPAttribute as $attribute) {
            if($attribute->name == $ldap_drupal_reverse_mappings[$key]) {
              $info[$field] = $attribute->values->string;
              break;
            }
          }
        }
      }
    }

    // Only update the profile if there is data to update
    if((module_exists('profile')) && (!empty($info))) {
      // Load the configured profile categories
      $categories = profile_categories();
      if(!empty($categories)) {
        // Loop through the categories
        foreach($categories as $index=> $category) {
          // Retrieve the fields for each category
          $result = _profile_get_fields($category['name'],false);
          $wanted = array();
          while ($profile_field = db_fetch_object($result)) {
            // Only map in the fields for this category
            $wanted[$profile_field->name] = $info[$profile_field->name];
          }
          // Update the user's profile
          profile_save_profile($wanted, $user, $category['name'], false);
        }
      }
    }
  }
  watchdog('crowd', 'Updated user %name with data from Crowd User with directory id %crowd_id', array('%name' => $crowd_user->name, '%crowd_id' => $crowd_user->directoryId), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $user->uid .'/edit'));
}