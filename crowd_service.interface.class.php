<?php
interface crowd_service{
  /**
   *  The authorize method should take the $username and $password as two
   *  strings and return an associative array keyed for the token.
   *
   *  @example An example of the object data structure
   *    array(1) { ["token"]=> string(24) "x3mbFRw2WdaA0D8tSorWJw00" }
   */
  public function authorize($username, $password);

  /**
   *  The logout method should take the Crowd token, delete it form Crowd and
   *  return TRUE or FALSE depending on if the action was successful.
   */
  public function logout($crowd_token);

  /**
   *  The user_get method should take the Crowd token, and return a keyed
   *  associative array that must contain at least the following values or FALSE.
   *
   *  @example An example of the returned array.
   *    array(9) {
   *      ["username"]=> string(8) "jpwarren"
   *      ["email"]=> string(25) "jpwarren@notrealemail.com"
   *      ["active"]=> string(4) "true"
   *    }
   */
  public function user_get($crowd_token);

  /**
   *  The crowd_is_logged_in method should take a Crowd token and return TRUE or
   *  FALSE.
   */
  public function crowd_is_logged_in($user_token);

  /**
   *  The list_groups method takes nothing in and returns all known Crowd groups
   *  as an indexed array.
   *
   *  @example  An example of the returned array.
   *    array(3) {
   *      [0]=> string(10) "developers"
   *      [1]=> string(15) "content-editors"
   *      [2]=> string(15) "content-authors"
   *    }
   */
  public function list_groups();

  /**
   *  The list_user_groups method takes the username and returns an indexed
   *  array.
   *
   *  @example  An example of the returned array.
   *    array(3) {
   *      [0]=> string(16) "confluence-users"
   *      [1]=> string(15) "jira-developers"
   *      [2]=> string(10) "jira-users"
   *    }
   */
  public function list_user_groups($name);
}